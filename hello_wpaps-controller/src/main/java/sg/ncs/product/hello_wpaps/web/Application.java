package sg.ncs.product.hello_wpaps.web.web;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@EnableFeignClients("sg.ncs.product.hello_wpaps.web.*")
@SpringBootApplication(scanBasePackages = "sg.ncs.product.hello_wpaps.web.*")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
